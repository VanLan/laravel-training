<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(UserSeeder::class);
       $this->call( CategorySeeder::class);
       $this->call(SizeSeeder::class);
       $this->call(ProductSeeder::class);
       $this->call(Product_detail_Seeder::class);
        $this->call(TestimonySeeder::class);
    }
}
