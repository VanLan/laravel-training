<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'phone' => '0123456789',
            'password' => bcrypt('admin'),
            'dob' => Carbon::now(),
            'gender' => 1,
            'role' => 'admin'
        ], [
            'name' => 'customer',
            'email' => 'customer@gmail.com',
            'phone' => '0123456788',
            'password' => bcrypt('customer'),
            'dob' => Carbon::now(),
            'gender' => 1,
            'role' => 'customer'
        ], [
            'name' => 'manager',
            'email' => 'manager@gmail.com',
            'phone' => '0123456787',
            'password' => bcrypt('manager'),
            'dob' => Carbon::now(),
            'gender' => 1,
            'role' => 'manager'
        ]]);
    }
}
