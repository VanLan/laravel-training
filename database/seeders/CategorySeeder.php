<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Str;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cate = ['T-shirt', 'Shoes', 'Bag', 'Neck', 'Watch'];
        for ($i = 4; $i >= 0; $i--){
            DB::table('categories')->insert([
                'name' => $cate[$i],
                'image' => ''
            ]);
        }
    }
}
