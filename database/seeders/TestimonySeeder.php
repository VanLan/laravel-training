<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestimonySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 3; $i++){
            DB::table('testimony')->insert([
                'user_id' => $i,
                'comment' => 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.'
            ]);
        }
    }
}
