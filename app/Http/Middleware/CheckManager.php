<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $rote)
    {
        $check = [
            "admin"=>['admin'], 
            "manager"=>['admin','manager']
        ];            
        if (!in_array(auth()->user()->role, $check[$rote]))
            abort(403);
        return $next($request); 
    }
}
