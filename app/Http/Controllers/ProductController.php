<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Size;
use App\Models\Categories;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate(10);
        $data['products'] = $products;
        return view('admin.product.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        $sizes = Size::all();
        $data['categories'] = $categories;
        $data['sizes'] = $sizes;
        return view('admin.product.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|min:6|max:20',
            'cate_id.*' => 'required|min:1|max:5',
            'price.*' => 'required|numeric',
        ]);
        try {
            DB::beginTransaction();
            //dd($request->hot); //lưu thông tin lấy về vào database, kiểm tra dữ liệu (ko null, price là số)
            $product = new Product();
            $product->name = $request->name;
            $product->cate_id = $request->category;
            $product->image = $request->image;
            $product->save();
            for ($i = 0; $i < count($request->size); $i++) {
                $price = str_replace(".", "", $request->price[$i]);
                $dataInsert = [
                    'product_id' => $product->id,
                    'size_id' => $request->size[$i],
                    'price' => $price
                ];
                $datas[$i] = $dataInsert;
            }
            ProductDetail::insert($datas);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
        return redirect()->back()->with('message', 'Data added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $sizes = Size::all();
        $data['product'] = $product;
        $data['sizes'] = $sizes;
        return view('admin.product.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $data['product'] = $product;
        return view('admin.product.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $product = Product::find($id);
            if (!$product) {
                abort(404);
            }
            $product->name = $request->name;
            $product->image = $request->image;
            $product->save();
            $product_detail = ProductDetail::where("product_id", $id)->delete();
            for ($i = 0; $i < count($request->size); $i++) {
                $price = str_replace(".", "", $request->price[$i]);
                $dataInsert = [
                    'product_id' => $product->id,
                    'size_id' => $request->size[$i],
                    'quantity' => $request->quantity[$i],
                    'price' => $price
                ];
                $datas[$i] = $dataInsert;
            }
            ProductDetail::insert($datas);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
        return redirect()->back()->with('message', 'Data updated sucess!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $product_detail = ProductDetail::find($id);
            $product = Product::find($product_detail->product_id);
            $product_detail->delete();
            if (count($product->details) == 0) {
                $product->delete();
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'message' => "Delete success"
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => "Delete success"
            ]);

        }

    }
}
