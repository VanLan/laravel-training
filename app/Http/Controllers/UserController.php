<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        $data['users'] = $users;
        return view('admin.user.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'checkpassword' => 'required',
                'dob' => 'required',
            ]);
            $user = new User();
            if ($request->password != $request->checkpassword){
                return redirect()->back()->with('message', 'Password does not match');
            }else{
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->dob = $request->dob;
                $user->gender = $request->gender;
                $user->role =$request->role;
                Mail::to($user->email)->send(new wellcom($user));
                $user->save();
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
        return redirect()->back()->with('message', 'Data added sucess!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $data['user'] = $user;
        return view('admin.user.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $user = User::find($id);
            if (!$user){
                abort(404);
            }
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password != null){
                $user->password = bcrypt($request->password);
            }else{
                $user->password = $user->password;
            }
            $user->gender = $request->gender;
            $user->role =$request->role;
            $user->save();
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
        return redirect()->back()->with('message', 'Data updated sucess!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $user = User::find($id)->delete();
            DB::commit();
            return response()->json([
                'error' => false,
                'message' => "Delete success"
            ]);
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => "Delete success"
            ]);

        }
    }
}
