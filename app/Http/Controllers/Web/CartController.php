<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
        if (!$product) {
            abort(404);
        }
        $cart = new Cart();
        $result = $cart->addToCart($product, $request->size, $request->quantity);
        if (!$result) {
            return redirect()->back()->with(['error' => 'Can not add item to cart']);
        }
        return redirect()->back()->with(['success' => 'Data added successfully!']);
    }

    public function index()
    {
        $cart = new Cart();
        return view('web.pages.cart')->with('cart', $cart);
    }

    public function delete($product_id, $size_id)
    {
        $cart = new Cart();
        $carts = $cart->cart;
        if ($carts[$product_id][$size_id]) {
            session()->forget('cart.' . $product_id . '.' . $size_id);
            if (session()->get('cart.' . $product_id) == null) {
                session()->forget('cart.' . $product_id);
            }
            if (session()->get('cart') == null) {
                session()->forget('cart');
            }
            return response()->json([
                'error' => false,
                'message' => "Delete success"
            ]);
        }
        return response()->json([
            'error' => true,
            'message' => "Delete don't success"
        ]);
    }

    public function update(Request $request)
    {
        try {
            $cart = new Cart();
            for ($i = 0; $i < count($request->id_size); $i++) {
                $cartItem = $cart->cart[$request->id_product[$i]][$request->id_size[$i]];
                $cartItem['quantity'] = $request->quantity[$i];
                session()->put('cart.' . $request->id_product[$i] . '.' . $request->id_size[$i], $cartItem);
            }
            return redirect()->route('checkout');
        } catch (\Exception $ex) {
            return redirect()->back()->with(['error' => 'Error']);
        }
    }
}
