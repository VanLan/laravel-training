<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductDetail;
use Illuminate\Http\Request;

class SingleController extends Controller
{
    public function detail($id)
    {
        $products = Product::limit(4)->get();
        $product = Product::find($id);
        $data['product'] = $product;
        $data['products'] = $products;
        return view('web.pages.single')->with($data);
    }

    public function getPrice($product_id, $size_id)
    {
        $product = Product::find($product_id);
        return response()->json([
            'price' => $product->getPriceOfSize($size_id),
            'quantity' => $product->getQuantityOfSize($size_id)
        ]);
    }


}
