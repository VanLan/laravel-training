<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductDetail;
use App\Models\Size;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class CheckoutController extends Controller
{
    public function index()
    {
        $carts = new Cart();
        return view('web.pages.checkout')->with('cart', $carts);
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'username' => 'required',
            'city' => 'required',
            'district' => 'required',
            'wards' => 'required',
            'village' => 'required',
            'phone' => 'required|digits:10|numeric',
            'email' => 'required|email:rfc,dns'
        ]);
        try {
            DB::beginTransaction();
            $users = User::select('id')
                ->where('email', $request->email)
                ->orwhere('phone', $request->phone)
                ->first();

            if ($users) {
                $id = $users->id;
            } else {
                $user = new User();
                $user->name = $request->username;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->gender = 1;
                $user->role = 'customer';
                $user->save();
                $id = $user->id;
            }
            $cart = new Cart();
            $order = new Order();
            $order->total = $cart->getTotal();
            $order->customer_id = $id;
            $order->status = 1;
            $order->order_date = date('Y-m-d H:m:s');
            $order->receiver_addr = $request->village . ', ' . $request->wards .
                ', ' . $request->district . ', ' . $request->city;
            $order->save();
            $i = 0;
            foreach ($cart->cart as $carts) {
                foreach ($carts as $cartItem) {
                    $product_detail = ProductDetail::where('product_id', $cartItem['id_product'])
                        ->where('size_id', $cartItem['id_size'])
                        ->first();
                    $dataInserts = [
                        'product_detail_id' => $product_detail->id,
                        'order_id' => $order->id,
                        'product_name' => $cartItem['name'],
                        'product_price' => $cartItem['price'],
                        'product_size' => Size::find($cartItem['id_size'])->name,
                        'qty' => $cartItem['quantity'],
                        'total' => $cartItem['quantity'] * $cartItem['price']
                    ];
                    $datas[$i] = $dataInserts;
                    /* update quantity product*/
                    $product_detail->quantity -= $cartItem['quantity'];
                    $product_detail->save();
                    $i++;
                }
            }
            OrderDetail::Insert($datas);
            DB::commit();
            session()->forget('cart');
            return redirect()->route('home')->with(['success' => 'Data added successfully!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with(['error' => "Data added don't successfully"]);
        }
    }
}
