<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index(Request $request){
        $category = $request->category;
        $products = Product::when($request->category != null, function  ($query) use ($category){
            $query->whereHas('category', function ($query) use ($category){
               $query->where('name', $category);
            });
        })->orderBy('created_at', 'desc')->paginate(12);
        $categories = Categories::all();
        $data['products'] = $products;
        $data['categories'] = $categories;
        $data['category_name'] = $request->category;
        return view('web.pages.list')->with($data);
    }
}
