<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Testimony;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::limit(4)->get();
        $products = Product::limit(8)->get();
        $testimonys = Testimony::limit(8)->get();
        $data['products'] = $products;
        $data['categories'] = $categories;
        $data['testimonys'] = $testimonys;
        return view('web.home')->with($data);
    }

    public function likeProduct($id)
    {
        $product = Product::find($id);
        $product->rate += 1;
        $product->save();
        return redirect()->back()->withCookie(cookie()->forever($product->id, $product->id));
    }
    public function unLikeProduct($id)
    {
        $product = Product::find($id);
        $product->rate -= 1;
        $product->save();
        Cookie::queue(Cookie::forget($product->id));
        return redirect()->back();
    }

}
