<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';

    public function details() {
        return $this->hasMany(ProductDetail::class, 'product_id', 'id');
    }

    public function category(){
        return $this->belongsTo(Categories::class, 'cate_id', 'id');
    }

    public function minPrice() {
        return number_format($this->details->min('price'), 0, ',', '.');
    }

    public function maxPrice() {
        return number_format($this->details->max('price'), 0, ',', '.');
    }
    public function getPriceOfSize($id_size){
        $product = ProductDetail::where('product_id', $this->id)
            ->where('size_id', $id_size)
            ->first();
        return $product->price;
    }
    public function getQuantityOfSize($id_size){
        $product = ProductDetail::where('product_id', $this->id)
            ->where('size_id', $id_size)
            ->first();
        return $product->quantity;
    }
}
