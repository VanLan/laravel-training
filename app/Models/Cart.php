<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    public $cart;
    public function __construct()
    {
        $this->cart = session()->get('cart') ?? [];
    }
    public function addToCart(Product $product, $size_id, $quantity){
        $currentQty = $product->getQuantityOfSize($size_id);
        if (isset($this->cart[$product->id][$size_id])) {
            $buyQty = $this->cart[$product->id][$size_id]['quantity'] + $quantity;
            if ($currentQty < $buyQty) {
                return false;
            }
            $this->cart[$product->id][$size_id]['quantity'] = $buyQty;
        } else {
            $this->cart[$product->id][$size_id] = [
                'id_product' => $product->id,
                'name' => $product->name,
                'image' => $product->img,
                'price' => $product->getPriceOfSize($size_id),
                'id_size' => $size_id,
                'quantity' => $quantity,
            ];
        }
        session()->put('cart', $this->cart);
        return true;

    }
    public function getTotal(){
        $totalBill = 0;
        foreach ($this->cart as $carts){
            foreach ($carts as $cart){
                $totalBill += $cart['quantity'] * $cart['price'];
            }
        }
        return $totalBill;
    }
}
