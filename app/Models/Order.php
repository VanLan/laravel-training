<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $guarded = [];
    public static $status = [
        "1" => "New",
        "2" => "Warning",
        "3" => "Done"
        ];
    public function user(){
        return $this->belongsTo(User::class, "customer_id");
    }
    public function details(){
        return $this->hasMany(OrderDetail::class, "order_id", "id");
    }
}
