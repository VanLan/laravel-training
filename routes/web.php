<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderDetailController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TestimonyController;
use \App\Http\Controllers\CategoriesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Web page */
/*View*/
Route::get('/', [\App\Http\Controllers\Web\HomeController::class, 'index'])->name('home');

Route::get('/shop', [\App\Http\Controllers\Web\ShopController::class, 'index'])->name('shop');
Route::get('/cart', [\App\Http\Controllers\Web\CartController::class, 'index'])->name('cart');
Route::get('/about', function () {
    return view('web.pages.about');
})->name('about');
Route::get('/contact', function () {
    return view('web.pages.contact');
})->name('contact');

Route::get('/like/{id}', [\App\Http\Controllers\Web\HomeController::class, 'likeProduct'])->name('like');
Route::get('/unlike/{id}', [\App\Http\Controllers\Web\HomeController::class, 'unLikeProduct'])->name('unlike');

/*Single page*/
Route::get('/product/{id}', [\App\Http\Controllers\Web\SingleController::class, 'detail']);
Route::get('/singe/{product_id}/{size_id}', [\App\Http\Controllers\Web\SingleController::class, 'getPrice']);
Route::post('/add/cart/{id}', [\App\Http\Controllers\Web\CartController::class, 'addToCart']);
/*Shop page*/
Route::get('/shop/{id}', [\App\Http\Controllers\web\ShopController::class, 'shopCategory'])->name('shopCategory');
/*Cart page*/
Route::delete('/cart/delete/{product_id}/{size_id}', [\App\Http\Controllers\Web\CartController::class, 'delete']);
Route::post('/cart/update', [\App\Http\Controllers\Web\CartController::class, 'update']);
/*Checkout page*/
Route::get('/cart/checkout', [\App\Http\Controllers\web\CheckoutController::class, 'index'])->name('checkout');
Route::post('/cart/create', [\App\Http\Controllers\web\CheckoutController::class, 'create'])->name('checkoutCreate');
/*=======================*/


/* Admin page */
Route::get('/admin/login', function () {
    return view('auth.login');
})->name('login');

Route::post('/admin/login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('auth.login');

Route::middleware(['auth'])->group(function () {
    Route::group(['middleware' => 'checkRole:admin'], function () {
        Route::get('/admin/', function () {
            return view('admin.dashboard');
        });
        Route::resource('/admin/order', OrderController::class);
        Route::get('/admin/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('auth.logout');
        Route::delete('/admin/delete/{id}', [ProductController::class, 'destroy']);
        Route::resource('/admin/user', UserController::class);
        Route::resource('/admin/category', CategoriesController::class);
        Route::resource('/admin/testimony', TestimonyController::class);
    });
    Route::group(['middleware' => 'checkRole:manager'], function () {
        Route::resource('/admin/product', ProductController::class);
    });
});
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
