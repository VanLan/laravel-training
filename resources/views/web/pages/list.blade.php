@extends('web.layouts.main', ['title' => 'Shop page'])
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Products</span>
                    </p>
                    <h1 class="mb-0 bread">Products</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10 mb-5 text-center">
                    <ul class="product-category">
                        <li><a href="/shop" class="{{$category_name == null ? 'active':''}}">All</a></li>
                        @foreach($categories as $category)
                            <li><a href="/shop?category={{$category->name}}"
                                   class="{{$category_name == $category->name ? 'active':''}}">{{$category->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="row">
                @foreach($products as $pro)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            <a href="/product/{{ $pro->id }}" class="img-prod"><img class="img-fluid"
                                                                                    src="../images/product-{{ $pro->id }}.jpg"
                                                                                    alt="Colorlib Template">
                                <span class="status">30%</span>
                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="/product/{{ $pro->id }}" class="d-inline-block text-truncate"
                                       style="width: 100%">{{ $pro->name }}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price"><span class="mr-2">₫{{ $pro->minPrice() }}</span> - <span
                                                class="price-sale">₫{{ $pro->maxPrice() }}</span></p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        @if(Cookie::get($pro->id) == $pro->name)
                                            <a href="{{route('unlike', $pro->id)}}"
                                               class="heart d-flex justify-content-center align-items-center ">
                                                <span>{{$pro->rate}}</span>
                                            </a>
                                        @else
                                            <a href="{{route('like', $pro->id)}}"
                                               class="heart d-flex justify-content-center align-items-center ">
                                                <span><i class="ion-ios-heart"></i></span>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="d-flex justify-content-center">
                        {{$products->links('web.layouts.pagelist')}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
