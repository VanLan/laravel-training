@extends('web.layouts.main', ['title' => $product->name])
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('../images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span class="mr-2"><a
                                href="index.html">Product</a></span> <span>Product Single</span></p>
                    <h1 class="mb-0 bread">Product Single</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-5 ftco-animate">
                    <a href="/" class="image-popup"><img src="../images/product-{{$product->id}}.jpg" class="img-fluid"
                                                         alt="Colorlib Template"></a>
                </div>
                <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                    <h3>{{$product->name}}</h3>
                    <div class="rating d-flex">
                        <p class="text-left mr-4">
                            <a href="#" class="mr-2">5.0</a>
                            <a href="#"><span class="ion-ios-star-outline"></span></a>
                            <a href="#"><span class="ion-ios-star-outline"></span></a>
                            <a href="#"><span class="ion-ios-star-outline"></span></a>
                            <a href="#"><span class="ion-ios-star-outline"></span></a>
                            <a href="#"><span class="ion-ios-star-outline"></span></a>
                        </p>
                        <p class="text-left mr-4">
                            <a href="#" class="mr-2" style="color: #000;">100 <span
                                    style="color: #bbb;">Rating</span></a>
                        </p>
                        <p class="text-left">
                            <a href="#" class="mr-2" style="color: #000;">500 <span style="color: #bbb;">Sold</span></a>
                        </p>
                    </div>
                    <p class="price"><span></span></p>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It
                        is a paradisematic country, in which roasted parts of sentences fly into your mouth. Text should
                        turn around and return to its own, safe country. But nothing the copy said could convince her
                        and so it didn’t take long until.
                    </p>
                    <form method="POST" action="/add/cart/{{$product->id}}">
                        @csrf
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <div class="form-group d-flex">
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="size" id="size" class="form-control">
                                            @foreach($product->details as $detail)
                                                <option value="{{$detail->size_id}}">{{$detail->size->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="input-group col-md-10 d-flex mb-3">
                                <span class="input-group-btn mr-2">
                                    <button type="button" class="quantity-left-minus btn minus" data-type="minus"
                                            data-field="">
                                        <i class="ion-ios-remove"></i>
                                    </button>
                                </span>
                                <input type="text" id="qty" name="quantity" class="form-control input-number" value="1">
                                <span class="input-group-btn ml-2">
                                    <button type="button" class="quantity-right-plus btn plus" data-type="plus"
                                            data-field="">
                                        <i class="ion-ios-add"></i>
                                    </button>
                                </span>
                                <div class="d-flex align-items-center ml-2 quantity"><span></span>&nbspsản phẩm có sẵn</div>
                            </div>
                            <div class="w-100"></div>
                        </div>
                        <p><input type="submit" class="btn btn-black py-3 px-5" value="Add to Cart"/></p>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Products</span>
                    <h2 class="mb-4">Related Products</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach($products as $pro)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            <a href="/product/{{ $pro->id }}" class="img-prod"><img class="img-fluid"
                                                                                    src="../images/product-{{ $pro->id }}.jpg"
                                                                                    alt="Colorlib Template">
                                <span class="status">30%</span>
                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="/product/{{ $pro->id }}" class="d-inline-block text-truncate"
                                       style="width: 100%">{{ $pro->name }}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price"><span class="mr-2">₫{{ $pro->minPrice() }}</span> - <span
                                                class="price-sale">₫{{ $pro->maxPrice() }}</span></p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        @if(Cookie::get($pro->id) == $pro->id)
                                            <a href="{{route('unlike', $pro->id)}}"
                                               class="heart d-flex justify-content-center align-items-center ">
                                                <span>{{$pro->rate}}</span>
                                            </a>
                                        @else
                                            <a href="{{route('like', $pro->id)}}"
                                               class="heart d-flex justify-content-center align-items-center ">
                                                <span><i class="ion-ios-heart"></i></span>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
@push('script')
    <script type="text/javascript">
        var currencyQty;


        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'VND',
            maximumFractionDigits: 0,
        });


        $(document).ready(function () {
            getPrice()
        })
        $('#size').on('change', function () {
            getPrice()
        })


        $('.minus').on('click', function () {
            let qty = parseInt($('#qty').val());
            if (qty > 1) {
                $('#qty').val(qty - 1);
            }
        })

        $('.plus').on('click', function () {
            let qty = $('#qty').val();
            qty = parseInt(qty) + 1;
            if(qty > currencyQty)
                $('#qty').val(currencyQty);
            else
                $('#qty').val(qty);
        })

        $('#qty').on('keyup', function () {
            if($('#qty').val() > currencyQty){
                $('#qty').val(currencyQty);
            }
        })
        function getPrice() {
            $.ajax({
                url: '/singe/' + {{$product->id}} + '/' + $('#size').val(),
                type: 'GET',
                success: function (result) {
                    console.log(result.price);
                    $('.price span').text(formatter.format(result.price));
                    $('.quantity span').text(result.quantity);
                    currencyQty = result.quantity;
                }
            })
        }
    </script>
@endpush
