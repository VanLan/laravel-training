@extends('web.layouts.main', ['title' => 'Checkout Page'])
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('../images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Checkout</span>
                    </p>
                    <h1 class="mb-0 bread">Checkout</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <form method="POST" action="/cart/create">
            @csrf
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-7 ftco-animate">
                        <form action="#" class="billing-form">
                            <h3 class="mb-4 billing-heading">Billing Details</h3>
                            <div class="row align-items-end">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastname">Full Name</label>
                                        <input type="text"
                                               class="form-control {{$errors->has('username') ? 'is-invalid' : ''}}"
                                               name="username" value="{{ old('username') }}" placeholder="">
                                        <div style="color: red">
                                            @error('username')
                                            {{$message}}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="streetaddress">Province/City</label>
                                        <input type="text"
                                               class="form-control {{$errors->has('city') ? 'is-invalid' : ''}}"
                                               name="city" value="{{ old('city') }}"
                                               placeholder="">
                                        <div style="color: red">
                                            @error('city')
                                            {{$message}}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="towncity">District</label>
                                        <input type="text"
                                               class="form-control {{$errors->has('district') ? 'is-invalid' : ''}}"
                                               name="district" placeholder="" value="{{ old('district') }}">
                                        <div style="color: red">
                                            @error('district')
                                            {{$message}}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Wards</label>
                                        <div class="select-wrap ">
                                            <input type="text"
                                                   class="form-control {{$errors->has('wards') ? 'is-invalid' : ''}}"
                                                   name="wards" value="{{ old('wards') }}">
                                            <div style="color: red">
                                                @error('wards')
                                                {{$message}}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Village</label>
                                        <div class="select-wrap">
                                            <input type="text"
                                                   class="form-control {{$errors->has('village') ? 'is-invalid' : ''}}"
                                                   name="village" value="{{ old('village') }}">
                                            <div style="color: red">
                                                @error('village')
                                                {{$message}}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text"
                                               class="form-control {{$errors->has('phone') ? 'is-invalid' : ''}}"
                                               placeholder="" name="phone" value="{{ old('phone') }}">
                                        <div style="color: red">
                                            @error('phone')
                                            {{$message}}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="emailaddress">Email Address</label>
                                        <input type="text"
                                               class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}"
                                               placeholder="" name="email" value="{{ old('email') }}">
                                        <div style="color: red">
                                            @error('email')
                                            {{$message}}
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form><!-- END -->
                    </div>
                    <div class="col-xl-5">
                        <div class="row mt-5 pt-3">
                            <div class="col-md-12 d-flex mb-5">
                                <div class="cart-total mb-3">
                                    <h3>Cart Totals</h3>
                                    <p class="d-flex">
                                        <span>Subtotal</span>
                                        <span class="subtotal"
                                              data-subtotal="{{$cart->getTotal()}}">₫{{number_format($cart->getTotal(), 0, '.', ',')}}</span>
                                    </p>
                                    <p class="d-flex">
                                        <span>Discount</span>
                                        <span class="discount" data-discount="0">₫0</span>
                                    </p>
                                    <hr>
                                    <p class="d-flex total-price">
                                        <span>Total</span>
                                        <span class="totalbill" data-totalbill="0"></span>
                                    </p>
                                    <p>
                                        <button type="submit" class="btn btn-primary py-3 px-4">Place an order</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .col-md-8 -->
                </div>
            </div>
        </form>
    </section> <!-- .section -->

    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('script')
    <script type="text/javascript">
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'VND',
            maximumFractionDigits: 0,
        });


        $(document).ready(function () {
            getTotalBill();
        })

        function getTotalBill() {
            const subtotal = parseInt($('.subtotal').data('subtotal'));
            let totalbill = subtotal - parseInt($('.discount').data('discount'));
            console.log(totalbill);
            $('.totalbill').text(formatter.format(totalbill));
            $('.totalbill').data('totalbill', formatter.format(totalbill));
        }
    </script>
@endpush
