@extends('web.layouts.main', ['title' => 'Cart page'])
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Cart</span></p>
                    <h1 class="mb-0 bread">My Cart</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-cart">
        <div class="container">
            @if(session()->has('cart'))
                <form action="/cart/update" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 ftco-animate">
                            <div class="cart-list">
                                <table class="table">
                                    <thead class="thead-primary">
                                    <tr class="text-center">
                                        <th>STT</th>
                                        <th>Product name</th>
                                        <th>Image</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $count = 0;@endphp
                                    @foreach($cart->cart as $key => $carts)
                                        @foreach($carts as $keyItem => $cartItem)
                                            <tr class="text-center cart-items item-{{$key}}">
                                                <td>{{++$count}} <input type="hidden" name="id_product[]"
                                                                        value="{{$key}}"><input type="hidden"
                                                                                                name="id_size[]"
                                                                                                value="{{$keyItem}}">
                                                </td>
                                                <td class="product-name">
                                                    <h3>{{$cartItem['name']}}</h3>
                                                </td>
                                                <td class="image-prod">
                                                    <div class="img"
                                                         style="background-image:url({{$cartItem['image']}});"></div>
                                                </td>
                                                <td>{{\App\Models\ProductDetail::find($cartItem['id_size'])->size->name}}</td>
                                                <td class="price" data-price="{{$cartItem['price']}}">
                                                    ₫{{number_format($cartItem['price'], 0, ',', '.')}}</td>

                                                <td class="col-2">
                                                    <div class="input-group mb-3">
                                                        <input type="number" name="quantity[]"
                                                               data-quantity="{{$cartItem['quantity']}}"
                                                               onchange="totalFunction({{$key}})"
                                                               class="form-control input-number qty"
                                                               value="{{$cartItem['quantity']}}">
                                                    </div>
                                                </td>

                                                <td class="total"
                                                    data-total="{{$cartItem['quantity'] * $cartItem['price']}}">
                                                    ₫{{number_format($cartItem['quantity'] * $cartItem['price'], 0, '.', ',')}}</td>
                                                <td class="product-remove"><a href="#"
                                                                              onclick="deleteCartItem({{$cartItem['id_product']}}, {{$key}})"><span
                                                            class="ion-ios-close"></span></a></td>
                                            </tr><!-- END TR-->
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-lg-8 mx-auto mt-5 cart-wrap ftco-animate" style="width: 100%">
                            <div class="cart-total mb-3">
                                <h3>Cart Totals</h3>
                                <p class="d-flex">
                                    <span>Subtotal</span>
                                    <span class="subtotal" data-subtotal="0">{{$cart->getTotal()}}</span>
                                </p>
                                <p class="d-flex">
                                    <span>Discount</span>
                                    <span class="discount" data-discount="0">₫0</span>
                                </p>
                                <hr>
                                <p class="d-flex total-price">
                                    <span>Total</span>
                                    <span class="totalbill" data-totalbill="0"></span>
                                </p>
                            </div>
                            <p><button type="submit" class="btn btn-primary py-3 px-4">Proceed to Checkout</button></p>
                        </div>
                    </div>
                </form>
            @else
                <div>
                    <h3 class="d-flex justify-content-center">GIỎ HÀNG CỦA BẠN RỖNG</h3>
                    <img src="images/emptycart.jpg" class="mx-auto d-block">
                </div>
            @endif
        </div>
    </section>

    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'VND',
            maximumFractionDigits: 0,
        });

        function totalFunction(id) {
            $('.item-' + id).find('.qty').data('quantity', $('.item-' + id).find('.qty').val())
            let quantity = parseInt($('.item-' + id).find('.qty').data('quantity'));
            let price = $('.item-' + id).find('.price').data('price');
            let total = quantity * price;
            $('.item-' + id).find('.total').text(formatter.format(total));
            $('.item-' + id).find('.total').data('total', total);
            getTotalBill();
        }

        $(document).ready(function () {
            getTotalBill();
        })

        function getTotalBill() {
            var total, subtotal = 0;
            $('.cart-items').each(function () {
                total = $(this).find('.total').data('total');
                subtotal += parseInt(total);
            })
            $('.subtotal').text(formatter.format(subtotal));
            $('.subtotal').data('subtotal', subtotal);

            let totalbill = subtotal - parseInt($('.discount').data('discount'))
            $('.totalbill').text(formatter.format(totalbill));
            $('.totalbill').data('totalbill', formatter.format(totalbill));
        }

        function deleteCartItem(id_product, id_size) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/cart/delete/' + id_product + '/' + id_size,
                            type: 'DELETE',
                            data: {
                                _token: '{{csrf_token()}}'
                            },
                            success: function (result) {
                                console.log(result.error);
                                if (!result.error) {
                                    swal("Good job!", "Delete product successfully!", "success")
                                        .then((value) => {
                                            location.reload();
                                        });
                                    console.log(result.name);
                                    return;
                                }
                                swal("Oh noes!", "Delete failed product!");

                            }
                        })
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }
    </script>
@endpush
