@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Product #{{ $product->id }}-{{ $product->name }}</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('product.update', $product)}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="productname">Product name</label>
                                <input type="text" class="form-control" id="productname" name="name"
                                       value="{{ $product->name }}"/>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                        <input id="thumbnail" class=" col-md-4 form-control" type="" name="image">
                                    </div>
                                    <img id="holder" style="margin-top:15px;max-height:100px;"
                                         src="{{$product->image}}">
                                </div>
                            </div>
                            <div id="add_item">
                                @php $check = true;
                                @endphp
                                @foreach($product->details as $detail)
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label for="size">Size</label>
                                            <select class="form-control" name="size[]">
                                                @foreach($sizes as $size)
                                                    <option
                                                        value="{{ $size->id }}" {{ $detail->size_id == $size->id ? "selected" : "" }}>{{ $size->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="price">Price</label>
                                            <input type="text" class="form-control price" name="price[]"
                                                   value="{{ number_format($detail->price, 0, ',', '.') }}"/>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="price">Quantity</label>
                                            <input type="text" class="form-control price" name="quantity[]"
                                                   value="{{$detail->quantity}}"/>
                                        </div>
                                        @if($check)
                                            @php $check = !$check; @endphp
                                            <div class="form-group col-md-1 d-flex align-items-end add-button">
                                                <button type="button" onclick="click_function()"
                                                        class="btn btn-success btn-outline js-addSize"
                                                        style="display: block;height: 40px;"><i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        @else
                                            <div class="form-group col-md-1 d-flex align-items-end">
                                                <button type="button"
                                                        class="btn_delete btn btn-danger btn-outline js-addSize"
                                                        style="display: block;height: 40px;"><i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a class="btn btn-default" href="{{ route('product.index') }}">Back</a>
                                    <button type="button" class="btn btn-default" onclick="remove_function()">Reset
                                    </button>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script type="text/javascript">
        $('#lfm').filemanager('file');
        $('#thumbnail').on('change', function (e) {
            $('#holder').attr('src', $(this).val());
        })


        const max_row = 10;
        const add_item = $("#add_item");

        function click_function() {
            let current_row = $('.form-row').length;
            if (current_row < max_row) {
                add_item.append('<div class="form-row">\n' +
                    '                                    <div class="form-group col-md-2">\n' +
                    '                                        <label for="size">Size</label>\n' +
                    '                                        <select class="form-control" name="size[]">\n' +
                    '                                            @foreach($sizes as $size)\n' +
                    '                                                <option value="{{ $size->id }}" >{{ $size->name }}</option>\n' +
                    '                                            @endforeach\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-2">\n' +
                    '                                        <label for="price">Price</label>\n' +
                    '                                        <input type="text" class="form-control price" name="price[]" value="0" />\n' +
                    '                                    </div>\n' +
                    '                                    <div class="form-group col-md-1 d-flex align-items-end btn_deletes">\n' +
                    '                                        <button type="button" class="btn_delete btn btn-danger btn-outline js-addSize" style="display: block;height: 40px;"><i class="fa fa-minus"></i></button>\n' +
                    '                                    </div>\n' +
                    '                                </div>');
                $(".btn_delete").click(function () {
                    if ($('.form-row').length > 1) {
                        $(this).closest('.form-row').remove();
                    }
                })
            }
        }

        $(document).ready(function () {
            $(".btn_delete").click(function () {
                if ($('.form-row').length > 1) {
                    $(this).closest('.form-row').remove();
                }
            })
        })

        function remove_function() {
            $(".price").attr('value', '');
            $('#productname').attr('value', '');
        }

    </script>
@endpush
