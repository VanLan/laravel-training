@extends('admin.layouts.main')
@section('content')
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item">
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create new user</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('user.store') }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">User name</label>
                            <input type="text" class="form-control" name="name" id="username" placeholder="Enter your user name" value="{{ old('name') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email" value="{{ old('email') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password" />
                        </div>
                        <div class="form-group">
                            <label for="checkpassword">Password again</label>
                            <input type="password" class="form-control" name="checkpassword" id="checkpassword" placeholder="Enter your password again" />
                        </div>
                        <div class="form-group">
                            <label for="dob">Birthday</label>
                            <input type="date" id="dob"class="form-control" name="dob">
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="1" checked>
                                <label class="form-check-label" for="gender">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="0">
                                <label class="form-check-label" for="inlineRadio2">Female</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="role" id="exampleRadios1" value="admin" checked >
                                <label class="form-check-label" for="role">
                                    admin
                                </label>
                                </div>
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="role" id="exampleRadios2" value="customer" >
                                <label class="form-check-label" for="role">
                                    customer
                                </label>
                                </div>
                                <div class="form-check disabled">
                                <input class="form-check-input" type="radio" name="role" id="exampleRadios3" value="manager" >
                                <label class="form-check-label" for="role">
                                    manager
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
