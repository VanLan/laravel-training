@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Order List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Customer</th>
                                <th>Order Date</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th>Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $count = 0;
                            @endphp
                            @foreach ($orders as $key => $order)
                                <tr>
                                    <td>{{++$count}}</td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{\Carbon\Carbon::parse($order->order_date)->format('j/m/Y') }}</td>
                                    <td><span class='badge badge-success'>{!! \App\Models\Order::$status[$order->status] !!}</span> </td>
                                    <td>{{number_format($order->total,0,',','.') }} VNĐ</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('order.show', $order) }}" title="Edit"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links('admin.Layout.pagelist') }}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection

