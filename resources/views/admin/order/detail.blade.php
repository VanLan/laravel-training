@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Order #{{ $order->id }}</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('order.update', $order)}}" method="POST">
                            @csrf
                            @method("PUT")
                            <table class="col-12">
                                <tr>
                                    <th class="col-3">Customer Name:</th>
                                    <td>{{$order->user->name}}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Email:</th>
                                    <td>{{$order->user->email}}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Date Order:</th>
                                    <td>{{Carbon\Carbon::parse($order->order_date)->format('d/m/Y')}}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Status:</th>
                                    <td>
                                        <!-- <select class="form-control col-2" name="status">
                                            @foreach(\App\Models\Order::$status as $key => $stt)
                                                <option
                                                    value="{{$key}}" {{$order->status == $key ? 'selected':''}}>{{$stt}}</option>
                                            @endforeach
                                        </select> -->
                                        <a href="#" id="status" data-type="select" data-pk="{{$order->id}}"
                                           data-title="Select status"></a>
                                    </td>
                                </tr>
                            </table>
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Product Name</th>
                                    <th>Product Size</th>
                                    <th>Product Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($order->details as $key => $detail)
                                    <tr class="item-{{ $detail->id }}" data-id="{{ $detail->id }}">
                                        <td>{{++$key}}</td>
                                        <td>{{$detail->product_name}}
                                            <input type="hidden" value="{{$detail->id}}" name="details_id[]"/>
                                        </td>
                                        <td>{{$detail->product_size}}</td>
                                        <td class="item-price" data-price="{{ $detail->product_price }}">
                                            {{number_format($detail->product_price, 0, ',', '.')}}
                                            VNĐ
                                        </td>
                                        <td>
                                            <!-- <input class="item-qty" type="number" value="{{$detail->qty}}"
                                                   name="quantity[]"> -->
                                            <a href="#" data-pk="{{$detail->id}}" data-value="{{$detail->qty}}" class="qty"></a>
                                        </td>

                                        <td class="item-total" data-itemtotal="{{ $detail->total }}">{{number_format($detail->total, 0, ',', '.')}}
                                            VNĐ
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tr>
                                    <th colspan="5">Total Bill:</th>
                                    <td id="total_bill" data-total="{{ $order->total }}">{{number_format($order->total, 0, ',', '.')}} VNĐ</td>
                                </tr>
                            </table>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a class="btn btn-default" href="{{ route('order.index') }}">Back</a>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script type="text/javascript">
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.params = function (params) {
            params._token = '{{ @csrf_token() }}';
            return params;
        };
        $('.qty').editable({
            type: 'number',
            url: '{{route("order.update", $order)}}',
            success: function(response, newValue) {
                if (response.status == 'error')
                    return response.msg; //msg will be shown in editable form
                console.log(response);
                $('.item-' + response.idDetail).find('.item-total').text(formatCurrency(response.itemTotal));
                $('#total_bill').text(formatCurrency(response.totalOrder));
            }
        });
        $('#status').editable({
            value: {{$order->status}},
            source: [
                    @foreach(\App\Models\Order::$status as $key => $stt)
                {
                    value: {{$key}}, text: '{{$stt}}'
                },
                @endforeach
            ],
            name: "status",
            url: '{{route("order.update", $order)}}',
            success: function (response, newValue) {
                if (response.status == 'error')
                    return response.msg; //msg will be shown in editable form
            }
        });
        function formatCurrency(number) {
            return number.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,') + " VNĐ";
        }
    </script>
@endpush
