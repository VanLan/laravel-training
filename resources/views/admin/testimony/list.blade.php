@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Testimony List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <td>Id User</td>
                                <th>User Name</th>
                                <th>Date Comment</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($testimonys as $key => $testimony)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$testimony->users->id}}</td>
                                    <td>{{$testimony->users->name}}</td>
                                    <td>{{$testimony->created_at->format('d/m/Y')}}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('testimony.show', $testimony) }}"
                                           title="Edit"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    {{$testimonys->links('admin.Layout.pagelist')}}
    <!-- /.row -->
    </div>
@endsection
