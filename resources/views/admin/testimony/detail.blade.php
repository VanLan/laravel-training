@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Testimony #{{$testimony->id}} - {{$testimony->users->name}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="productname">User Name</label>
                            <p class="form-control">{{$testimony->users->name}}</p>
                        </div>
                        <div class="form-group">
                            <label for="productname">Date Comment</label>
                            <p class="form-control">{{$testimony->created_at->format('d/m/Y')}}</p>
                        </div>
                        <div class="form-group">
                            <label for="productname">Comment Conten</label>
                            <p style="border: 1px solid black; text-align: justify; padding: 10px; border-radius: 10px">{{$testimony->comment}}</p>
                        </div>
                        <div class="d-flex justify-content-between">
                            <a class="btn btn-default" href="{{ route('testimony.index') }}">Back</a>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection
