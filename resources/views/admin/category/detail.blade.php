@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Category #{{ $category->id }}-{{ $category->name }}</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('category.update', $category)}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="productname">Product name</label>
                                <input type="text" class="form-control" id="productname" name="name"
                                       value="{{ $category->name }}"/>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                        <input id="thumbnail" class=" col-md-4 form-control" type="" name="image">
                                    </div>
                                    <img id="holder" style="margin-top:15px;max-height:100px;"
                                         src="{{$category->image}}">
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <a class="btn btn-default" href="{{ route('category.index') }}">Back</a>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script type="text/javascript">
        $('#lfm').filemanager('file');
        $('#thumbnail').on('change', function (e) {
            $('#holder').attr('src', $(this).val());
        })
    </script>
@endpush
