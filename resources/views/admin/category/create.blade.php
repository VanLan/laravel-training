@extends('admin.layouts.main')
@section('content')
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item" style="color:#ec4844">
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create new product</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('category.store') }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group col-md-4">
                            <label>Category Name </label>
                            <input type="text" value="{{ old('name') }}" name="name" class="form-control"
                                   placeholder="Please input category name">
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                <input id="thumbnail" class=" col-md-4 form-control" type="" name="image"
                                       value="{{ old('image') }}">
                            </div>
                            <img id="holder" style="margin-top:15px;max-height:100px;">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script type="text/javascript">
        {{--        image--}}
        $('#lfm').filemanager('file');
        $('#thumbnail').on('change', function (e) {
            $('#holder').attr('src', $(this).val());
        })
    </script>
@endpush
