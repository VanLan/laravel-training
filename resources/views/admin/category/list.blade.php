@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Product List</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Category Name</th>
                                <th>Image Category</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $count = 0;
                            @endphp
                            @foreach ($categories as $key => $category)
                                <tr>
                                    <td>{{++$count}}</td>
                                    <td>{{ $category->name }}</td>
                                    <td><img src="{{$category->image}}" style="width: 70px; height: 70px"></td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('category.show', $category) }}"
                                           title="Edit"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger" href="#" onclick="deleteCategory({{$category->id}})"
                                           title="Delete"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    {{$categories->links('admin.Layout.pagelist')}}
    <!-- /.row -->
    </div>
@endsection
@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function deleteCategory(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '/admin/category/' + id,
                            type: 'DELETE',
                            data: {
                                _token: '{{csrf_token()}}'
                            },
                            success: function (result) {
                                console.log(result.error);
                                if (!result.error) {
                                    swal("Good job!", "Delete product successfully!", "success")
                                        .then((value) => {
                                            location.reload();
                                        });
                                    console.log(result.name);
                                    return;
                                }
                                swal("Oh noes!", "Delete failed product!");

                            }
                        })
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }
    </script>
@endpush
